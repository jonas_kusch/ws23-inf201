import numpy as np

def relu_scalar(x):
    if x > 0:
        return x
    else:
        return 0
relu = np.vectorize(relu_scalar)

class relu_layer:
    def __init__(self, n, m):
        self.W = np.ones((n, m))
        self.b = np.ones((n))

    def layer_eval(self, x):
        return relu(self.W @ x + self.b)
    
    def read_layer(self, nameW, nameb):
        pass

class lin_layer:
    def __init__(self, n, m):
        self.W = np.ones((n, m))
        self.b = np.ones((n))

    def layer_eval(self, x):
        return self.W @ x + self.b
    
    def read_layer(self, nameW, nameb):
        pass

class network:
    def __init__(self, n, m):
        self.my_layer = relu_layer(n, m)
        self.my_second_layer = lin_layer(10, n)
    def eval(self, x):
        y1 = self.my_layer.layer_eval(x)
        y2 = self.my_second_layer.layer_eval(y1)
        return y2

m = 81
n = 100
x = np.random.rand(m)
nn = network(n, m)
print(nn.eval(x))
