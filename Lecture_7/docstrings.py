class layer:
    """ Layer of a neural network """
    def __init__(self, n, m):
        """ Constructor initializes weight matrix and bias vector.
            n : output dimension of layer
            m : input dimension of layer
        """
        self._W = np.ones((n, m))
        self._b = np.ones((n))

print(layer.__doc__)
print(layer.__init__.__doc__)
