class animal:
    def __init__(self):
        self.nlegs = 4
        self.number = 0
    def info():
        print("I am an animal")

class cat(animal):
    def __init__(self, n):
        super().__init__()
        self.number = n
    def info(self):
        print("I am a cat")

c = cat(10)
c.info()
print(c.number)
print(c.nlegs)
