class parent:
    def __init__(self, data):
        self.data = data
    def parent_print(self):
        print(self.data)

class child(parent):
    def __init__(self, data):
        super().__init__(data + 1)
        #self.data = data
    def print(self):
        print(self.data)

child(1).parent_print()