import json

# dictionary of two students
students = [{'Name': 'Per', 'Age': 24, 'Phone number': '12345678'},
            {'Name': 'Kari', 'Age': 23, 'Phone number': '23456789'}]

# write information to json file
with open('students.json', 'w') as jfile:
    json.dump(students, jfile)

# print out file
print(open('students.json').read())

# read file
students_from_json = json.load(open('students.json'))
print(students_from_json)