import pandas as pd
import matplotlib.pyplot as plt

# read data
weather12 = pd.read_csv('weather_umb_2012.csv', encoding='latin-1', sep=';', parse_dates=[0], dayfirst=True)

print(weather12.info())
