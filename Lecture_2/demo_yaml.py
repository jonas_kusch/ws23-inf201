import yaml

# dictionary of two students
students = [{'Name': 'Per', 'Age': 24, 'Phone number': '12345678'},
            {'Name': 'Kari', 'Age': 23, 'Phone number': '23456789'}]

# write information to yaml file
with open('students.yml', 'w') as jfile:
    yaml.dump(students, jfile)

# print out file
print(open('students.yml').read())

# read file
students_from_yaml = yaml.safe_load(open('students.yml'))
print(students_from_yaml)