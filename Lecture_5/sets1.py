numbers = range(21)
odds = {n for n in numbers if n % 2 == 1}
primes = {2, 3, 5, 7, 11, 13, 17, 19}

print('odds   :', odds)
print('primes :', primes)

# combining checking intersections
odd_primes = odds & primes

# odds without primes
odd_non_primes = odds - primes

print(odd_primes)
print(odd_non_primes)
