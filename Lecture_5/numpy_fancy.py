import numpy as np

b = np.array([0, 1])
W = np.matrix([[0, 1], [2, 3]])

# solve linear system
x = np.linalg.solve(W, b)
print(np.linalg.norm(W @ x - b))

# compute QR
Q, R = np.linalg.qr(W)
print(np.linalg.norm(W - Q @ R))

# compute SVD
U, S, V = np.linalg.svd(W)
print(np.linalg.norm(W - U @ np.diag(S) @ V))
