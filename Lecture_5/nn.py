import numpy as np

def sigma(y):
        return y
sigma_vec = np.vectorize(sigma)

def layer(W, b, x):
    return sigma_vec(W @ x + b)

def f(x, W_1, b_1):
    return layer(W_1, b_1, x)

W_1 = np.random.rand(10, 64)
b_1 = np.random.rand(10)
x = np.random.rand(64)

print(f(x, W_1, b_1))