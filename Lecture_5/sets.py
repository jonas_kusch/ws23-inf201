s = set([1, 2, 3])

# equivalent to
s = {1, 2, 3}

print(s)

s2 = {n for n in range(10) if n % 3 > 0}
print(s2)

numbers = range(20)
odds = {n for n in numbers if n % 2}
evens = {n for n in numbers if n % 2 == 0}
primes = {2, 3, 5, 7, 11, 13, 17, 19}
squares = {n**2 for n in numbers if n**2 < max(numbers)}

print('odds   :', odds)
print('evens  :', evens)
print('primes :', primes)
print('squares:', squares)

if 5 in odds:
    print("5 is odd!")

#  combining sets
odds_and_evens = odds | evens

print(odds_and_evens)

# combining checking intersections
print(odds & evens)
print(odds & primes)

# odds without primes
print(odds - primes)

# printing
print(list(odds_and_evens)[0])

for i in odds_and_evens:
    print(i, end = ", " )
