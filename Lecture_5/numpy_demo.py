import numpy as np

x = np.array([0, 1])

print(x)
print(2*x**2)

# there are several functions that you can use!
print('x.ndim  :', x.ndim)
print('x.size  :', x.size)
print('x.shape :', x.shape)
print('x.dtype :', x.dtype)
print('x.nbytes:', x.nbytes)

# ways to create a vector
z = np.linspace(1, 50)
print(z)
z = np.linspace(0, 1, num=11)
print(z)

# now matrices
W = np.matrix([[0, 1], [2, 3]])

# matrix vector multiplication
print(W.dot(x))

print(W @ x)

# mulitply first column only
print(W[:,0].T @ x)

print(np.linalg.norm(W))
