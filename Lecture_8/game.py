from random import randint

class player:
    def __init__(self, id):
        self.id = id
        self.position = 1

    def move(self):
        self.position += randint(1, 6)

    def go_back(self):
        self.position -= 4

class game:
    def __init__(self, num_players):
        self.return_positions = range(5, 31, 5)
        self.players = [player(i) for i in range(num_players)]

    def play(self):
        while True:
            for player in self.players:
                player.move()
                if player.position in self.return_positions:
                    player.go_back()
                if player.position >= 31:
                    return player.id
            
g = game(4)
print(f"Player {g.play()} wins!")