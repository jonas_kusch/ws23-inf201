from random import randint
from abc import ABC, abstractmethod

class player(ABC):
    def __init__(self, id):
        self.id = id
        self.position = 1

    @abstractmethod
    def move(self, reroll=False):
        pass

    def go_back(self):
        self.position -= 4

class reroll_player(player):
    def __init__(self, id):
        super().__init__(id)

    def move(self):
        up = randint(1, 6)
        if up < 3:
            up = randint(1, 6)
        self.position += up

class normal_player(player):
    def __init__(self, id):
        super().__init__(id)

    def move(self):
        up = randint(1, 6)
        self.position += up

class game:
    def __init__(self, num_players):
        self.return_positions = range(5, 31, 5)
        self.players = [normal_player(i) for i in range(num_players)]
        self.players[0] = reroll_player(0)

    def play(self):
        while True:
            for player in self.players:
                player.move()
                if player.position in self.return_positions:
                    player.go_back()
                if player.position >= 31:
                    return player.id

g = game(4)
print(f"Player {g.play()} wins!")
