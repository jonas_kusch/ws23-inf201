import numpy as np
import sys

class layer:
    def __init__(self, n, m):
        self._W = np.random.random((n, m))
        self._b = np.random.random(n)
        assert self._W.shape[0] == self._b.shape[0], "Dimensions do not match."

class network:
    def __init__(self, n_list, m_list):
        self._layers = []
        n_prev = m_list[0]
        for n, m in zip(n_list, m_list):
            try:
                if m != n_prev:
                    raise AssertionError("Dimensions of layers not matching.")
                n_prev = n
                self._layers.append(layer(n, m))
            except AssertionError as err:
                print("Error observed:")
                print(err)
                #sys.exit(1)

n_list = [5, 6]
m_list = [4, 4]

nn = network(n_list, m_list)
print("Program finished")
