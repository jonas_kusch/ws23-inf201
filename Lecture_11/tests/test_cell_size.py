import pytest
from simple_mesh import *

def test_cell_size_positive():
    c = line(0, [1, 2])
    assert c.cell_size() == pytest.approx(1)

def test_cell_size_negative():
    c = line(0, [2, 1])
    assert c.cell_size() == pytest.approx(1)