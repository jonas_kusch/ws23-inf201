import torch
from torch.autograd import Variable

# Define the functions
def f(x):
    return x**2 + 2*x + 1

def g(x, epsilon):
    return epsilon*x

# Create a variable with requires_grad=True so that PyTorch can compute gradients
x = Variable(torch.tensor([0.0]), requires_grad=True)

# Forward pass: compute the function value
y = f(x)

# Backward pass: compute the gradient of the function with respect to x
y.backward()
print(x.grad)

# Forward pass: compute the function value
x.grad.zero_()
y = g(x, 0.0)

# Backward pass: compute the gradient of the function with respect to x
y.backward()
print(x.grad)