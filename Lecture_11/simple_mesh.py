import numpy as np
from abc import ABC, abstractclassmethod

class point:
    def __init__(self, value, index) -> None:
        self._value = value
        self._index = index

class cell(ABC):
    def __init__(self, cell_id, points) -> None:
        self._id = cell_id
        self._points = points

    @abstractclassmethod
    def cell_size(self):
        pass

    @property
    def id(self):
        return self._id

class line(cell):
    def __init__(self, line_id, points) -> None:
        super().__init__(line_id, points)
    def cell_size(self):
        return self._points[1] - self._points[0]
    
class boundary_point(cell):
    def __init__(self, point_id, points) -> None:
        super().__init__(point_id, points)
    def cell_size(self):
        return 0

point_values = np.linspace(0, 1, 100)
points = []
for idx, pt in enumerate(point_values):
    points.append(point(pt, idx))

cells = []
for idx, (pt1, pt2) in enumerate(zip(point_values[:-1], point_values[1:])):
    cells.append(line(idx, [pt1, pt2]))
cells.append(boundary_point(len(cells), point_values[0]))
cells.append(boundary_point(len(cells), point_values[-1]))

for c in cells:
    print(f"Cell {c.id}: size is {c.cell_size()}")