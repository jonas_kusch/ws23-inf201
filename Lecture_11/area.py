import math

def calc_area(r):
    """Returns area of circle with radius r."""
    try:
        if r < 0:
            raise ValueError("No numbers below zero")
        return math.pi * r**2
    except ValueError as err:
        print(err)

calc_area(-0.1)
print("finished")
