import numpy as np

class layer:
    def __init__(self, n, m):
        self._W = np.random.random((n, m))
        self._b = np.random.random(n)
    @property
    def W(self):
        return self._W
    @property
    def b(self):
        return self._b


my_lay = layer(5, 6)
print(my_lay.W)
my_lay.W = 1
