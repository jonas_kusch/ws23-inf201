def show_steps(func):

    def wrapped_func(*args, **kwargs):

        print('Args:', *args, **kwargs)

        res = func(*args, **kwargs)

        print('Res :', res)

        return res

    return wrapped_func

@show_steps
def fib(n):
    if n <= 2:
        return 1
    else:
        return fib(n-1) + fib(n-2)

print(fib(5))
