def change_input(func):
    def wrapped_func(*args, **kwargs):
        res = func(3, **kwargs)
        return res
    return wrapped_func

@change_input
def fib(n):
    if n <= 2:
        return 1
    else:
        return fib(n-1) + fib(n-2)

print(fib(5))
