import numpy as np

class layer:
    def __init__(self, n, m):
        self._W = np.random.random((n, m))
        self._b = np.random.random(m)
        assert self._W.shape[0] == self._b.shape[0], "Dimensions do not match."


my_lay = layer(5, 6)
