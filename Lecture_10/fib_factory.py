def fib(n):
    if n <= 2:
        return 1
    else:
        return fib(n-1) + fib(n-2)

def show_steps(func):

    def wrapped_func(*args, **kwargs):

        print('Args:', *args, **kwargs)

        res = func(*args, **kwargs)

        print('Res :', res)

        return res

    return wrapped_func

fib = show_steps(fib)

print(fib(5))
