def make_multiplier(factor):
    
    def multiplier(n):
        return factor * n
    
    return multiplier

doubler = make_multiplier(2)
tripler = make_multiplier(3)

factor = 10

print(doubler(2), doubler(5), doubler('a'))
print(tripler(2), tripler(5), tripler('a'))