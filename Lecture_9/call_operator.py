import numpy as np

class lin_layer:
    def __init__(self, n, m):
        self.W = np.ones((n, m))
        self.b = np.ones((n))
    
    def __call__(self, x):
        return self.W @ x + self.b

n_out = 10    
n_in = 64
x = np.random.random(n_in)
layer_1 = lin_layer(n_out, n_in)
print(layer_1(x))