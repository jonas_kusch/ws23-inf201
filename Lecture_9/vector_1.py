class vector:
    def __init__(self, n, data=0):
        self._data = [data] * n
        self._n = n

    def __add__(self, other):
        c = vector(self._n)
        for i, (d1, d2) in enumerate(zip(self._data, other._data)):
            c._data[i] = d1 + d2
        
        return c

a = vector(5, 4)
b = vector(5, 1)

c = a + b

print(c._data)

