class data:
    def __init__(self, data=1):
        self._data = data

    def __add__(self, other):
        self._data += self._data
        return self

    def __mul__(self, other):
        self._data *= self._data
        return self

    def __str__(self):
        return str(2*self._data)

print((data() + data(2))*data(3))