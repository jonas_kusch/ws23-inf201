class vector:
    def __init__(self, n, data=0):
        self._data = [data] * n
        self._n = n

# add two vectors
a = vector(5, 4)
b = vector(5, 1)
c = vector(5)
for i, (d1, d2) in enumerate(zip(a._data, b._data)):
    c._data[i] = d1 + d2

print(c._data)