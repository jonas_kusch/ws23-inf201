import numpy as np
from abc import ABC, abstractmethod
from copy import deepcopy
from torchvision import datasets, transforms

def get_mnist():
    return datasets.MNIST(root='./data', train=True, transform=transforms.ToTensor(), download=True)

def return_image(image_index, mnist_dataset):
    # Get the image and its corresponding label
    image, label = mnist_dataset[image_index]

    # Now, you have the image as a PyTorch tensor.
    # You can access its data as a matrix using .detach().numpy()
    image_matrix = image[0].detach().numpy()  # Grayscale image, so we select the first channel (index 0)

    return image_matrix.reshape(image_matrix.size), image_matrix, label

# implementation of the relu function
def relu_scalar(x):
    if x > 0:
        return x
    else:
        return 0
relu = np.vectorize(relu_scalar)

class abstract_layer(ABC):
    """Abstract layer class: Every layer needs to inherit from abstract layer to make sure the call function is defined."""
    def __init__(self, n, m):
        """Constructor builds layer with given dimensions"""
        self.W = np.ones((n, m))
        self.b = np.ones((n))

    @abstractmethod
    def __call__(self, x):
        pass

    """Read weight and bias from file"""
    def read(self, nameW, nameb):
        with open(nameW) as file:
                for j, line in enumerate(file):
                    self.W[j, :] = [float(value) for value in line.strip().split()]

        with open( nameb ) as file:
            self.b = [float(value) for value in file.read().strip().split()]

class sin_layer(abstract_layer):
    """Layer with sinusoidal activation"""
    def __init__(self, n, m):
        super().__init__(n, m)

    def __call__(self, x):
        return np.sin(self.W @ x + self.b)

class relu_layer(abstract_layer):
    """Layer with relu activation"""
    def __init__(self, n, m):
        super().__init__(n, m)

    def __call__(self, x):
        return relu(self.W @ x + self.b)

class lin_layer(abstract_layer):
    """Layer with linear activation"""
    def __init__(self, n, m):
        super().__init__(n, m)

    def __call__(self, x):
        return self.W @ x + self.b

class network:
    def __init__(self, n):
        self._layers = []
        for i, (n_cur, n_next) in enumerate(zip(n[:-1], n[1:])):
            nameW = "network/W_" + str(i+1) + ".txt"
            nameb = "network/b_" + str(i+1)  + ".txt"
            if i < len(n) - 2:
                l = relu_layer(n_next, n_cur)
            else:
                l = lin_layer(n_next, n_cur)
            l.read(nameW, nameb)
            self._layers.append(l)

    def __call__(self, x):
        z = deepcopy(x)
        for layer in self._layers:
            z = layer(z)
        return z
    
nn_architecture = [ 
    {"type" : "relu", "size" : (512, 784)},
    {"type" : "relu", "size" : (256, 512)},
    {"type" : "lin", "size" : (10, 256)} ]

n_inputs = 784
n_outputs = 10
n = [n_inputs, 512, 256, n_outputs]
n = [784, 512, 256, 10]

# Choose an index to select one of the images
image_index = 19961
mnist_dataset = get_mnist()
x, image, label = return_image(image_index, mnist_dataset)

f = network(n)
y = f(x)
print(y / np.linalg.norm(y))

print(label)
