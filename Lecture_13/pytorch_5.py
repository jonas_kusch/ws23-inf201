import torch
from torch.autograd import Variable

def f(x, a):
    return a + 2*x

x = Variable(torch.tensor([1.0]), requires_grad=False)
a = Variable(torch.tensor([3.0]), requires_grad=True)
y = f(x, a)
y.backward()
print(a.grad)