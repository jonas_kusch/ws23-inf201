import torch
from torch.autograd import Variable
import torch.optim as optim

# Define the function you want to minimize
def f(x):
    return x[0]**2 + 0.9*x[1]**2

# Create a variable with requires_grad=True so that PyTorch can compute gradients
x = Variable(torch.tensor([0.9, 0.9]), requires_grad=True)

# Choose an optimization algorithm (e.g., stochastic gradient descent)
optimizer = optim.SGD([x], lr=0.1)

# Number of iterations
num_iterations = 100

# Optimization loop
for i in range(num_iterations):
    # Forward pass: compute the function value
    y = f(x)

    # Backward pass: compute the gradient of the function with respect to x
    y.backward()

    # Update x using the optimizer
    optimizer.step()

    # Zero the gradients for the next iteration
    optimizer.zero_grad()

# Print the final result
print("Minimum value of f(x) is:", f(x).item())
print("Value of x at the minimum:", x)
