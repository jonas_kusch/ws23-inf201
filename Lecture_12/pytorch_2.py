import torch
from torch.autograd import Variable
import numpy as np

def evaluate(x, W, b):
    return torch.matmul(W, x) + b

def f(x, W, b):
    return torch.norm(evaluate(x, W, b))

m = 16**2
n = 10
W = Variable(torch.randn(n, m), requires_grad=False)
b = Variable(torch.randn(n), requires_grad=False)

# Create a variable with requires_grad=True so that PyTorch can compute gradients
x = Variable(torch.randn(m), requires_grad=True)

# Forward pass: compute the function value
y = f(x, W, b)

# Backward pass: compute the gradient of the function with respect to x
y.backward()
print(x.grad)