import torch
from torch.autograd import Variable
import torch.optim as optim

# Define the function you want to minimize
def f(x):
    return x[0]**2 + 0.9*x[1]**2

# Create a variable with requires_grad=True so that PyTorch can compute gradients
x = Variable(torch.tensor([0.9, 0.9]), requires_grad=True)

# Number of iterations
num_iterations = 100
lr = 0.1
for i in range(num_iterations):
    y = f(x)
    y.backward()

    x.data = x - lr * x.grad
    x.grad.zero_()

# Print the final result
print("Minimum value of f(x) is:", f(x).item())
print("Value of x at the minimum:", x)
