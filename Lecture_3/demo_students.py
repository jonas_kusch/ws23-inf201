students = []
with open('students.txt') as f:
    headers = f.readline().rstrip('\n').split(';')
    for line in f:
        record = line.rstrip('\n').split(';')
        d = {}
        for header, data in zip(headers, record):
            d[header] = data
        students.append(d)

print(students)