import re
from pathlib import Path

folder = Path('.') / "simulation_data"
nan_reg = re.compile(r'.*NaN.*')

for file in folder.glob("*.txt"):
    with open(file) as f:
        for line_number, line in enumerate(f):
            out = nan_reg.findall(line)
            if len(out) > 0:
                print("NaN in ", file, " line ", line_number)
