from pathlib import Path
cwd = Path.cwd()
#cwd = Path.cwd() + "demo_pathlib_0.py"
#cwd = Path.cwd() / "demo_pathlib_0.py"

print(cwd)
print(cwd.exists())
print(cwd.is_dir())
print(cwd.is_file())
print(cwd.is_absolute())
print(cwd.as_uri())