from pathlib import Path
cwd = Path.cwd()

# print parent directory
print(cwd.parent)

# print listof parent directories
print(list(cwd.parents))

# create relative path...
relpath = Path('.')
print(relpath)

# ...and turn it into an absolute path
print(relpath.resolve())