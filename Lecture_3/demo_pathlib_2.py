from pathlib import Path
cwd = Path.cwd()

lecture_2 = Path('..') / 'Lecture_2'
print(lecture_2.resolve())

if lecture_2.resolve().is_dir():
    print("This folder exists")

for item in lecture_2.glob('*.tx?'):
    if item.is_file():
        print('FILE', end='')
    elif item.is_dir():
        print('DIR ', end='')
    else:
        print('????', end='')
    print(item)

print(list(lecture_2.glob('*.test')))
# `**` is a wildcard indicating recursives search through subdirectories
print(list(lecture_2.glob('**/*.test')))