from pathlib import Path
import re
cwd = Path.cwd()

r_prog = re.compile(r'\bTitus\b')

for path in Path('..').glob('Lecture_[123]/*.py'):
    if path.is_file():
        with path.open() as file:
            for line in file:
                if r_prog.search(line):
                    print(f'{str(path):20s}: {line[:50]}')
