import re

titus_regex = re.compile(r'\A(\w*)\s\w*')

out = titus_regex.findall("Titus is a cat.")

print(out)

out = titus_regex.sub("Richie", "Titus is a cat.")

print(out)