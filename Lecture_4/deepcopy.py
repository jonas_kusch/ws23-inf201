from copy import deepcopy

l = [11, [0, 0], 33, 44]
m = l.copy()
m[1][0] = 1
print(l)
print(m)

l = [11, [0, 0], 33, 44]
m = deepcopy(l)
m[1][0] = 1
print(l)
print(m)
