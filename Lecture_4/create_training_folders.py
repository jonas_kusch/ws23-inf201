from pathlib import Path
import yaml

demo_dir = Path('demo')
if not demo_dir.exists():
    demo_dir.mkdir()

testcases = ["CIFAR" + str(i) for i in range(20, 120, 20)]
learning_rates = [10**(-i) for i in range(1,4)]

for tc in testcases:
    sd = demo_dir / str(tc)
    if not sd.exists():
        sd.mkdir()
    for lr in learning_rates:
        setup = {"learning_rate": lr, "test_case": tc, "n_epochs": 100}
        with open(sd / ('setup_'+str(lr)+'.txt'), 'w') as jfile:
            yaml.dump(setup, jfile)

files = list(demo_dir.glob('**/*'))
for f in files:
    print(f)
