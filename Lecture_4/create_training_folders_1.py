from pathlib import Path

demo_dir = Path('demo')

for old_name in demo_dir.glob('*/*.txt'):
    name = str(old_name)[:-4]
    old_name.rename(name + ".yml")