def add(A, B):
    C = [ [ 0 for i in range(1,4) ] for j in range(2, 5)]
    for i, (row_a, row_b) in enumerate(zip(A, B)):
        for j, (a, b) in enumerate(zip(row_a, row_b)):
            C[i][j] = a + b
    return C            

A = [ [ i + j for i in range(1,4) ] for j in range(2, 5)]
B = [ [ i + j for i in range(1,4) ] for j in range(2, 5)]

C = add(A, B)
print(C)
