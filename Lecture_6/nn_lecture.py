import numpy as np

class layer:
    def __init__(self, n, m):
        self.W = np.ones((n, m))
        self.b = np.ones((n))

    def layer_eval(self, x):
        return self.W @ x + self.b
    
    def read_layer(self, nameW, nameb):
        # read in weight W and bias b
        pass

class network:
    def __init__(self, n, m):
        self.my_layer = layer(n, m)
        self.my_second_layer = layer(10, n)
    def eval(self, x):
        # make this more general
        y1 = self.my_layer.layer_eval(x)
        y2 = self.my_second_layer.layer_eval(y1)
        return y2
    def read():
        # go over all layers in the network and call the function read
        pass

m = 81
n = 100
x = np.random.rand(m)
nn = network(n, m)
print(nn.eval(x))
