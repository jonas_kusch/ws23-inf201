import numpy as np

class layer:
    def __init__(self, n, m):
        print(f"constructor with dimension {n}, {m}")
        self.W = np.random.rand(n, m)
        self.b = np.random.rand(n)

def layer_eval(l, x):
    return l.W @ x + l.b

l1 = layer(100,64)
l2 = layer(10,100)
x = np.random.rand(64)

y = layer_eval(l1, x)
y = layer_eval(l2, y)