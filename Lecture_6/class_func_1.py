import numpy as np

class layer:
    def __init__(self, n, m):
        self.W = np.random.rand(n, m)
        self.b = np.random.rand(n)

    def eval(self, x):
        return self.W @ x + self.b

x = np.random.rand(64)
l = layer(100, 64)
l.eval(x)